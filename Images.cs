﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.IO;

namespace Net_Neuralab_Utilities
{
    public class Images
    {
        public static string SavePublicImageToS3(string AKID, string S3Secret, string bucket, string name, MemoryStream file)
        {
            try
            {
                AmazonS3Config config = new AmazonS3Config
                {
                    UseSecureStringForAwsSecretKey = false
                };
                AmazonS3Client client = new AmazonS3Client(AKID, S3Secret, config);
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucket,
                    CannedACL = S3CannedACL.PublicRead,
                    AutoCloseStream = true,
                    Key = name,
                    InputStream = file
                };
                client.PutObject(request);
                return ("http://" + bucket + ".s3.amazonaws.com/" + name);
            }
            catch (Exception exception)
            {
                return exception.Message.ToString();
            }
        }
    }
}
